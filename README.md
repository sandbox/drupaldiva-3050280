Quick Overview of Framework

Elements --

Elements are the most fundamental items capture. Corresponding to HTML elements,
these classes contain html attributes as properties and methods to encapsulate 
functionality for these elements.

Components --

Components are made up of elements and other components. They encapsulate the 
properties and methods at the component level.

PageObjects --

PageObjects contain elements and components. They represent functionality at
the page level.

Tests --
Tests use a combination of PageObjects to implement test cases.

